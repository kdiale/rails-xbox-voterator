class AddIsSuperuserToUsers < ActiveRecord::Migration
  def change
    add_column :users, :is_superuser, :boolean, default: false
  end
end
