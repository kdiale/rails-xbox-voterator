require 'rails_helper'

RSpec.describe GamesController, :type => :controller do
  describe 'with games API loaded with games' do
    let(:games_api) { GamesAPI.new }

    before(:each) do
      @game1 = GamesAPI::Game.new({
        'title' => 'Sonic the Hedgehog',
        'id' => 1,
        'votes' => 1,
        'status' => 'wantit',
      })

      @game2 = GamesAPI::Game.new({
        'title' => 'Super Mario Bros 3',
        'id' => 2,
        'votes' => 1,
        'status' => 'gotit',
      })

      @game3 = GamesAPI::Game.new({
        'title' => 'Sonic the Hedgehog 2',
        'id' => 3,
        'votes' => 1,
        'status' => 'wantit',
      })

      @valid_games = [
        @game1,
        @game2,
      ]

      @valid_games_after_post = [
        @game1,
        @game2,
        @game3,
      ]

      @games_api = double()
      allow(GamesAPI).to receive(:new) { @games_api }
      allow(@games_api).to receive(:add_game) { true }
    end

    describe 'if I am logged in' do
      before(:each) do
        hash = {}
        allow(controller).to receive(:authenticate_user!) { true }
        allow(controller).to receive(:user_session) { hash }
      end

      it 'should allow me to add a new game' do
        post :create, {
          game: {
            title: 'Sonic the Hedgehog 2',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:notice]).to_not be(nil)
      end

      it 'should fail if I add a duplicate game' do
        allow(@games_api).to receive(:add_game) do
          raise GamesAPI::DuplicateGameException
        end

        post :create, {
          game: {
            title: 'Sonic the Hedgehog',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should fail if I add a game on the weekend' do
        allow(@games_api).to receive(:add_game) do
          raise GamesAPI::WeekendException
        end

        post :create, {
          game: {
            title: 'Sonic the Hedgehog 2',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should fail if I add a blank game title' do
        allow(@games_api).to receive(:add_game) do
          raise GamesAPI::EmptyGameException
        end

        post :create, {
          game: {
            title: '',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should allow me to cast a vote' do
        allow(@games_api).to receive(:add_vote) { true }

        post :update,{
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:notice]).to_not be(nil)
      end

      it 'should not allow me to vote on the weekend' do
        allow(@games_api).to receive(:add_vote) do
          raise GamesAPI::WeekendException
        end

        post :update, {
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should assign an error if parameters are omitted' do
        post :update, {
          id: 1,
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should not allow me to add a game twice in one day' do
        post :create, {
          game: {
            title: 'Sonic the Hedgehog 2',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:notice]).to_not be(nil)

        post :create, {
          game: {
            title: 'Sonic the Hedgehog 3',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(root_url)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should not allow me to vote on a game twice in one day' do
        allow(@games_api).to receive(:add_vote) { true }

        post :update,{
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:notice]).to_not be(nil)

        post :update,{
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(flash[:alert]).to_not be(nil)
      end

      it 'should not allow me to vote and add a game on the same day' do
        allow(@games_api).to receive(:add_vote) { true }

        post :update,{
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:notice]).to_not be(nil)

        post :create, {
          game: {
            title: 'Sonic the Hedgehog 3',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(root_url)
        expect(flash[:alert]).to_not be(nil)
      end

      describe 'and I am an admin' do
        before(:each) {
          user = double()

          allow(user).to receive(:is_superuser) { true }
          allow(controller).to receive(:current_user) { user }
        }

        it 'should allow me to mark a game as owned' do
          allow(@games_api).to receive(:set_got_it) { true }

          post :update, {
            id: 1,
            game_action: 'gotit',
          }

          expect(response.status).to eq(302)
          expect(response.location).to eq(owned_games_url)
          expect(flash[:notice]).to_not be(nil)
        end
      end

      describe 'and I am not an admin' do
        before(:each) {
          user = double()

          allow(request).to receive(:referrer) { games_url }
          allow(user).to receive(:is_superuser) { false }
          allow(controller).to receive(:current_user) { user }
        }

        it 'should not allow me to mark a game as owned' do
          allow(@games_api).to receive(:set_gotit) { true }

          post :update, {
            id: 1,
            game_action: 'gotit',
          }

          expect(response.status).to eq(302)
          expect(response.location).to eq(games_url)
          expect(flash[:alert]).to_not be(nil)
        end
      end
    end

    describe 'if I am not logged in' do
      before(:each) do
        allow(controller).to receive(:authenticate_user!) do
          controller.redirect_to user_session_path
        end
      end

      it 'should not allow me to add a new game' do
        post :create, {
          game: {
            title: 'Sonic the Hedgehog 2',
          }
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(user_session_url)
      end

      it 'should not allow me to mark a game as owned' do
        post :update, {
          id: 1,
          game_action: 'gotit',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(user_session_url)
      end

      it 'should not allow me to vote on a game' do
        post :update, {
          id: 1,
          game_action: 'vote',
        }

        expect(response.status).to eq(302)
        expect(response.location).to eq(user_session_url)
      end
    end
  end
end
