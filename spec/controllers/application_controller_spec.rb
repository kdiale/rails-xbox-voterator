require 'rails_helper'

RSpec.describe ApplicationController, :type => :controller do

  describe 'if_authorized' do
    describe 'if the user is not authorized' do
      controller do
        def index
          if_authorized(true) { true }
        end
      end

      it 'should execute block' do
        get :index

        allow(controller).to receive(:redirect_to) { true }
        expect { |b| controller.send('if_authorized', true, &b) }.to yield_control
      end
    end

    describe 'if the user is not authorized' do
      controller do
        def index
          if_authorized(false) { true }
        end
      end

      it 'should redirect to root_path' do
        get :index

        expect(response.status).to eq(302)
        expect(response.location).to eq(root_url)
        expect(flash[:alert]).not_to be(nil)
      end

      it 'should redirect to the previous page' do
        allow(controller.request).to receive(:referrer) { games_path }

        get :index

        expect(response.status).to eq(302)
        expect(response.location).to eq(games_url)
        expect(flash[:alert]).not_to be(nil)
      end

      it 'should not execute code block' do
        get :index

        allow(controller).to receive(:redirect_to) { true }
        expect { |b| controller.send('if_authorized', false, &b) }.not_to yield_control
      end
    end
  end

  describe 'if_user_may' do
    before(:each) do
      user_session = Hash.new

      allow(controller).to receive(:user_session) { user_session }
    end

    controller do
      def index
        if_user_may { true }
      end
    end

    it 'should not yield if the user has acted already today' do
      allow(controller).to receive(:redirect_to) { true }
      get :index

      expect(response.status).to eq(200)
      expect { |b| controller.send('if_user_may', &b) }.not_to yield_control
    end

    it 'should yield if the user has not acted today' do
      allow(controller).to receive(:redirect_to) { true }

      expect { |b| controller.send('if_user_may', &b) }.to yield_control
    end

    it 'should redirect to prior page if an action has already been performed' do
      allow(controller).to receive(:user_session) do
        {
          'last_action' => Date.current.to_s
        }
      end
      allow(controller.request).to receive(:referrer) { games_path }

      get :index

      allow(controller.request).to receive(:referrer) { games_path }

      expect(response.status).to eq(302)
      expect(response.location).to eq(games_url)
      expect(flash[:alert]).not_to be(nil)
    end

    it 'should redirect to root_path if no prior page and action performed' do
      allow(controller).to receive(:user_session) do
        {
          'last_action' => Date.current.to_s
        }
      end

      get :index

    expect(response.status).to eq(302)
    expect(response.location).to eq(root_url)
    expect(flash[:alert]).not_to be(nil)
    end
  end
end
