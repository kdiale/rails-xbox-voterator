require 'rails_helper'

RSpec.describe GamesAPI do
  let(:games_api) { GamesAPI.new }

  it 'should not allow blank games into the API' do
    expect { games_api.add_game('') }.to(
      raise_error(GamesAPI::EmptyGameException)
    )
  end

  describe 'with games in the API' do
    let(:games_api) { GamesAPI.new }

    before(:each) do
      @game1 = {
        'title' => 'Sonic the Hedgehog',
        'id' => 1,
        'votes' => 5,
        'status' => 'wantit',
      }

      @game2 = {
        'title' => 'Echo the Dolphin',
        'id' => 4,
        'votes' => 11,
        'status' => 'wantit',
      }

      @game3 = {
        'title' => 'Sonic the Hedgehog 3',
        'id' => 3,
        'votes' => 9,
        'status' => 'wantit',
      }

      @game4 = {
        'title' => 'Super Mario Bros 3',
        'id' => 2,
        'votes' => 1,
        'status' => 'gotit',
      }

      @game5 = {
        'title' => 'Super Mario Bros 2',
        'id' => 5,
        'votes' => 1,
        'status' => 'gotit',
      }

      @valid_games = [
        @game1,
        @game2,
        @game3,
        @game4,
        @game5,
      ]

      allow(games_api.class).to receive(:get) { @valid_games }
    end

    it 'should return array of GamesAPI::Game objects' do
      expect(games_api.games).to be_kind_of(Array)
      expect(games_api.games[0]).to be_kind_of(GamesAPI::Game)
      expect(games_api.games.count).to eq(5)
    end

    it 'should return games with a status of wantit' do
      expect(games_api.pending_games.collect { |game| game.status }).to all(
        eq('wantit')
      )
    end

    it 'should sort pending games by descending votes' do
      expect(games_api.pending_games[0].title).to eq(@game2['title'])
      expect(games_api.pending_games[1].title).to eq(@game3['title'])
    end

    it 'should return games with a status of gotit' do
      expect(games_api.owned_games.collect { |game| game.status }).to all(
        eq('gotit')
      )

      expect(games_api.owned_games[0].title).to eq(@game5['title'])
      expect(games_api.owned_games[1].title).to eq(@game4['title'])
    end

    describe 'attempting to add a game' do
      describe 'during the week' do
        before do
          Timecop.freeze(Time.local(2014, 10, 6, 0, 0))
        end

        after do
          Timecop.return
        end

        it 'should succeed' do 
          allow(games_api.class).to receive(:get) { true }

          expect(games_api.add_vote(1)).to eq(true)
        end
      end

      describe 'during the weekend' do
        before do
          Timecop.freeze(Time.local(2014, 10, 4, 0, 0))
        end

        after do
          Timecop.return
        end

        it 'should fail' do
          expect { games_api.add_vote(1) }.to(
            raise_error(GamesAPI::WeekendException)
          )
        end
      end
    end

    describe 'attempting to add a game' do
      describe 'during the week' do
        before do
          Timecop.freeze(Time.local(2014, 10, 6, 0, 0))
        end

        after do
          Timecop.return
        end

        it 'should fail duplicate games with the same case' do
          allow(games_api.class).to receive(:get) { true }
          allow(games_api).to receive(:games) do
            @valid_games.collect do |game|
              GamesAPI::Game.new(game)
            end
          end

          expect { games_api.add_game('Sonic the Hedgehog') }.to(
            raise_error(GamesAPI::DuplicateGameException)
          )
        end

        it 'should fail duplicate games with a different case' do
          allow(games_api.class).to receive(:get) { true }
          allow(games_api).to receive(:games) do
            @valid_games.collect do |game|
              GamesAPI::Game.new(game)
            end
          end

          expect { games_api.add_game('sonic the hedgehog') }.to(
            raise_error(GamesAPI::DuplicateGameException)
          )
        end

        it 'should allow totally different games' do
          allow(games_api.class).to receive(:get) { true }
          allow(games_api).to receive(:games) do
            @valid_games.collect do |game|
              GamesAPI::Game.new(game)
            end
          end

          expect(games_api.add_game('Sonic the Hedgehog 2')).to eq(true)
        end
      end

      describe 'during the weekend' do
        before do
          Timecop.freeze(Time.local(2014, 10, 4, 0, 0))
        end

        after do
          Timecop.return
        end

        it 'should fail' do
          allow(games_api.class).to receive(:get) { true }
          allow(games_api).to receive(:games) do
            @valid_games.collect do |game|
              GamesAPI::Game.new(game)
            end
          end

          expect { games_api.add_game('Sonic the Hedgehog 2') }.to(
            raise_error(GamesAPI::WeekendException)
          )
        end
      end
    end
  end

  describe 'without games in the API' do
    let(:games_api) { GamesAPI.new }

    before(:each) do
      allow(games_api.class).to receive(:get) { [] }
    end

    it 'should return an empty array' do
      expect(games_api.games).to be_kind_of(Array)
      expect(games_api.games.count).to eq(0)
    end

    it 'should allow adding any game' do
      allow(games_api.class).to receive(:get) { true }
      allow(games_api).to receive(:games) { [] }

      Timecop.freeze(Time.local(2014, 10, 13, 0, 0)) do
        expect(games_api.add_game('Sonic the Hedgehog')).to eq(true)
      end
    end
  end
end
