require 'httparty'

# Public: HTTParty class for interfacing with the remote server.
#
# Examples
#   games_api = GamesAPI.new
#   puts games_api.games
#   # => ['Sonic the Hedgehog', 'Super Mario Bros 3']
class GamesAPI
  attr_accessor :api_key

  include HTTParty

  base_uri 'http://js.november.sierrabravo.net/challenge'

  # Public: Initializes the API class and sets the secret key
  def initialize
    @api_key = Rails.application.secrets[:games_api_key]
  end

  # Public: Checks to see if the key is valid.
  #
  # Examples
  #   games_api = GamesAPI.new
  #   games_api.check_key?
  #   # => true
  #
  # Returns a boolean for whether the key is valid or not
  def check_key?
    self.class.get('/checkKey', query)
  end

  # Public: Pulls down an array of games from the API and converts them to
  # GamesAPI::Game instances for ease of use.
  #
  # Examples
  #     games_api = GamesAPI.new
  #     games_api.games.count
  #     # => 2
  #
  # Returns an array of GamesAPI::Game objects
  def games
    self.class.get('/getGames', query).map do |game|
      Game.new(game)
    end
  end

  # Public: Retrieves all games with a status of 'wantit' and sorts them in 
  # descending order by votes
  #
  # Returns an Array of GamesAPI::Game objects
  def pending_games
    unsorted = games.keep_if do |game|
      game.status == 'wantit'
    end

    unsorted.sort do |a, b|
      if a.votes < b.votes
        1
      elsif a.votes > b.votes
        -1
      else
        0
      end
    end
  end

  # Public: Retrieves all games with a status of 'gotit' and sorts them in
  # alphabetical order
  #
  # Returns an Array of GamesAPI::Game objects
  def owned_games
    unsorted = games.keep_if do |game|
      game.status == 'gotit'
    end

    unsorted.sort do |a, b|
      if a.title > b.title
        1
      elsif a.title < b.title
        -1
      else
        0
      end
    end
  end

  # Public: Adds a game to the api. Resultant game will have a status of 
  # 'wantit', 1 vote, and an id. It will fail if the game already exists in
  # the API or if today is a weekend.
  #
  # game - A string that represents the title of the game.
  #
  # Raises GamesAPI::EmptyGameException when an empty string is passed
  # Raises GamesAPI::WeekendException if it is called during the weekend
  # Raises GamesAPI::DuplicateGameException if the game is already in the API
  #
  # Example
  #   games_api = GamesAPI.new
  #   games_api.add_game('Sonic the Hedgehog')
  #   # => true
  #
  # Returns a boolean for whether or not the add was successful.
  def add_game(game)
    if game.empty?
      raise GamesAPI::EmptyGameException
    end

    if games.empty?
      games_set = Set.new
    else
      games_set = Set.new(games.collect { |game_obj| game_obj.title.downcase })
    end

    today = Time.now

    if today.sunday? || today.saturday?
      raise GamesAPI::WeekendException
    end

    if games_set.member? game.downcase
      raise GamesAPI::DuplicateGameException
    end

    self.class.get('/addGame', query(title: game))
  end

  # Public: Removes all games from the api.
  #
  # Examples
  #   games_api = GamesAPI.new
  #   games_api.clear_games
  #   games_api.games.count
  #   # => 0
  #
  # Returns a boolean for whether or not the operation was successful.
  def clear_games
    self.class.get('/clearGames', query)
  end

  # Public: Adds a vote to the game. Will fail on Saturday & Sunday.
  #
  # id - Integer id of the game to which you are adding the vote
  #
  # Raises GamesAPI::WeekendException if called during the weekend
  #
  # Returns boolean value representing the success of the operation.
  def add_vote(id)
    today = Time.now

    if today.sunday? || today.saturday?
      raise GamesAPI::WeekendException
    end

    self.class.get('/addVote', query(id: id))
  end

  # Public: Sets that a game is now owned.
  #
  # id - Integer id of the game of which you are changing the status
  #
  # Returns boolean representing the success of the operation.
  def set_got_it(id)
    self.class.get('/setGotIt', query(id: id))
  end

  # Public: Game object that provides a logical grouping for id, title, votes,
  # and status.
  #
  # Not to be used outside of the GamesAPI class in which it is nested.
  class Game
    attr_accessor :id, :title, :votes, :status, :api_class

    # Public: Initializes the GamesApi::Game object. Should only be called
    # by GamesAPI or in testing.
    #
    # params - Hash of values to set onto the object
    def initialize(params)
      @id = params['id']
      @title = params['title']
      @votes = params['votes']
      @status = params['status']
    end
  end

  private

  # Private: Utility function for keeping the api_key and merging in unique
  # parameters on a per call basis.
  #
  # params - A hash to be merged into the query. Optional.
  #
  # Returns a hash with any hash passed in via params merged into the query
  # key
  def query(params = {})
    { query: { apiKey: @api_key }.merge(params) }
  end

  class WeekendException < Exception
  end

  class DuplicateGameException < Exception
  end

  class EmptyGameException < Exception
  end
end
