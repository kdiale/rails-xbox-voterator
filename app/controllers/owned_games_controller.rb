class OwnedGamesController < ApplicationController
  before_action :initialize_games_api

  def index
    @games = @games_api.owned_games
  end

  private

  def initialize_games_api
    @games_api = GamesAPI.new
  end
end
