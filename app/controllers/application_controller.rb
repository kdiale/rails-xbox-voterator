class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception

  private

  # Public: If the condition evaluates to true, run the block passed.
  # Otherwise send the user back to either the referrer or the root with an
  # auth message.
  def if_authorized(condition)
    if condition
      yield
    else
      redirect_to request.referrer || root_path,
        alert: 'You are not authorized to perform that action'
    end
  end

  # Public: Checks if a user has already made an action before midnight of
  # today. If so, redirects to either the page they came from or to the root.
  #
  # Accepts a block that'll be executed if the user may execute it.
  def if_user_may
    if user_session['last_action'].nil? || !user_session['last_action'].to_date.today?
      yield
      user_session['last_action'] = Date.current
    else
      redirect_to request.referrer || root_path,
        alert: 'You may only vote or add a game once a day'
    end
  end
end
