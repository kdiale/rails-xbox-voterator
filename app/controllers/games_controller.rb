class GamesController < ApplicationController
  before_action :initialize_games_api
  before_action :authenticate_user!, only: [:create, :update]

  def index
    @games = @games_api.pending_games
  end

  def create
    begin
      if_user_may do
        @games_api.add_game(params[:game][:title])
        redirect_to games_path, notice: 'Game added successfully'
      end
    rescue GamesAPI::DuplicateGameException
      redirect_to games_path, alert: 'That game already exists in the API' 
    rescue GamesAPI::EmptyGameException
      redirect_to games_path, alert: 'The game title is blank'
    rescue GamesAPI::WeekendException
      redirect_to games_path, alert: 'You may not add a game on the weekend'
    end
  end

  def update
    if params[:game_action].nil?
      redirect_to games_path, alert: 'One or more parameters blank.'
    end

    if params[:game_action] == 'vote'
      if_user_may { add_vote(params[:id]) }
    elsif params[:game_action] == 'gotit'
      set_gotit(params[:id])
    end
  end

  private

  def initialize_games_api
    @games_api = GamesAPI.new
  end

  def add_vote(id)
    begin
      @games_api.add_vote(params[:id])
      redirect_to games_path, notice: 'Thanks for voting!'
    rescue GamesAPI::WeekendException
      redirect_to games_path, alert: (
        'You may not vote on games on the weekend'
      )
    end
  end

  def set_gotit(id)
    if_authorized(current_user.is_superuser) do
      @games_api.set_got_it(params[:id])
      redirect_to owned_games_path, notice: 'This game is now marked as owned'
    end
  end
end
